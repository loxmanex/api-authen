const {
    ApiResult
} = require('../utils/result.helper');

const {HttpResponse} = require('../configs/config');



exports.login = (req, db, res) => {

    let _apiResult = new ApiResult();

    let _email = req.body.email;
    let _password = req.body.password;

    let usersRef = db.collection('users');

    usersRef.where('email','==',_email).where('password','==',_password).limit(1).get()
    .then((snapshot) => {
        let user_auth = null;
        snapshot.forEach((doc) => {
            console.log(doc.id, '=>', doc.data());
            let _response_data = doc.data();
            _response_data.id = doc.id;
            user_auth = _response_data;
        });

        if(user_auth != null){
            _apiResult.data = user_auth.id;
            _apiResult.status = HttpResponse.OK;
        }else{
            _apiResult.status = HttpResponse.UNAUTHORIZED;
            _apiResult.message = HttpResponse.UNAUTHORIZED_MSG;
        }
        
        res.json(_apiResult); 
    })
    .catch((err) => {
        console.log('Error getting documents', err);
        _apiResult.status = HttpResponse.ERROR;
        _apiResult.message = HttpResponse.ERROR_MSG;
        res.json(_apiResult);
    });
}

exports.register = (req,db,res) => {
    let _apiResult = new ApiResult();
    let params = req.body;
    let _last_name = params.last_name;
    let _username = params.username;
    let _email = params.email;
    let _first_name = params.first_name;
    let _password = params.password;

    db.collection('users').add({
        last_name: _last_name,
        username: _username,
        email: _email,
        first_name: _first_name,
        password: _password
      }).then(ref => {
        console.log('Added document with ID: ', ref.id);
        _apiResult.status = HttpResponse.OK;
      }).catch((err) => {
        console.log('Error adding documents', err);
        _apiResult.status = HttpResponse.ERROR;
        _apiResult.message = HttpResponse.ERROR_MSG;
        res.json(_apiResult);
    });
}

exports.getProfile = (req, db, res) => {

    let _apiResult = new ApiResult();

    const _user_id = req.params.user_id;

    let usersRef = db.collection('users');

    usersRef.doc(_user_id).get()
    .then((doc) => {
            
        let _response_data = doc.data();
        _response_data.id = doc.id;

        _apiResult.status = HttpResponse.OK;
        _apiResult.data = _response_data;
        
        res.json(_apiResult); 
    })
    .catch((err) => {
        console.log('Error getting documents', err);
        _apiResult.status = HttpResponse.ERROR;
        _apiResult.message = HttpResponse.ERROR_MSG;
        res.json(_apiResult);
    });
}